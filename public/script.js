    
const download = document.getElementById('Save');    
    download.addEventListener('click', function(e) {

      html2canvas(document.getElementById('Page')).then(canvas => {    
        document.body.appendChild(canvas);
        canvas.style.display ="none";
        var link = document.createElement('a');
        var dataURL = canvas.toDataURL("image/png");
        link.download = 'image.png';
        link.href = dataURL; 
        document.body.appendChild(link);
        link.style.display ="none";  
        link.click();
        link.delete;
        canvas.delete;
      });
    }
    );
   


interact('.drag').draggable({
    // enable inertial throwing
    inertia: true,
    // keep the element within the area of it's parent
    modifiers: [
      interact.modifiers.restrictRect({
        restriction: 'parent',
        endOnly: true
      })
    ],
    // enable autoScroll
    autoScroll: true,

    listeners: {
      // call this function on every dragmove event
      move: dragMoveListener,

      // call this function on every dragend event
      
    }
  })

function dragMoveListener (event) {
  var target = event.target
  // keep the dragged position in the data-x/data-y attributes
  var x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx
  var y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy

  // translate the element
  target.style.transform = 'translate(' + x + 'px, ' + y + 'px)'

  // update the posiion attributes
  target.setAttribute('data-x', x)
  target.setAttribute('data-y', y)
}

// this function is used later in the resizing and gesture demos
window.dragMoveListener = dragMoveListener
